﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemRepoReflection
{
    interface IBaseRepository<T> where T : class, new()
    {
        void Add(T model);
        void Insert(int index, T model);
        void AddRange(IEnumerable<T> models);
        int SaveChanges();
        IEnumerable<T> AsEnumerable();
    }
}
