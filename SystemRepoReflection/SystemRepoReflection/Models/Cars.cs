﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemRepoReflection.Models
{
    class Cars
    {
        public int Price { get; set; }
        public string Model { get; set; }
        public string Marks { get; set; }
        public string Color { get; set; }

        public string FullInformation => $"{Marks} {Model} {Color}";
    }
}
