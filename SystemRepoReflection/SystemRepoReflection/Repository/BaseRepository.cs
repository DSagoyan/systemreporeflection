﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace SystemRepoReflection.Repository
{
     abstract  class BaseRepository<T> : IBaseRepository<T>
        where T : class, new()
    {
        protected BaseRepository()
        {
            _fileSet = AsEnumerable().ToList();
        }

        private IList<T> _fileSet;

        public abstract string FileName { get; }

        public void Add(T model)
        {
            _fileSet.Add(model);
        }

        public void AddRange(IEnumerable<T> models)
        {
            foreach (var model in models)
            {
                _fileSet.Add(model);
            }
        }

        public IEnumerable<T> AsEnumerable()
        {
            if (!File.Exists(FileName))
                File.Create(FileName).Dispose();

            StreamReader reader = File.OpenText(FileName);

            T item = null; 

            while(!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                switch (line)
                {
                    case "{":
                        item = new T();
                        break;
                    case "}":
                        yield return item;
                        break;
                    default:
                        FillModel (line, item):
                        break;
                }
            }
            reader.Dispose();
        }

        public void Insert(int index, T model)
        {
            _fileSet.Insert(index, model);
        }

        public int SaveChanges()
        {
            int count = 0;
            StreamWriter writer = File.CreateText(FileName);
            foreach (var car in _fileSet)
            {
                writer.WriteLine("{");
                WriteModel(writer, car);
                writer.WriteLine("}");

                count++;
            }
            _fileSet.Clear();
            writer.Dispose();
            return count;
        }

        private void FillModel (string line, T model)
        {
            string[] data = line.Split(':');
            if (data.Length > 2)
                return;
            OnFillModel(data, model);
        }

        private void OnFillModel (string[] data, T model)
        {
            string propName = data[0];
            var pi = model.GetType().GetProperty(propName, BindingFlags.Instance | BindingFlags.Public);
            if(pi != null)
            {
                object value = Convert.ChangeType(data[1], pi.PropertyType);
                pi.SetValue(model, value);
            }
        }

        public bool IsNullable(Type type)
        {
            return Nullable.GetUnderlyingType(type) != null;
        }

        private void WriteModel(StreamWriter writer, T model)
        {
            var pis = model.GetType().GetProperties();
            foreach (var pi in pis)
            {
                if (IsIgnored(pi.Name))
                    continue;

                    object value = pi.GetValue(model);
                if (pi.PropertyType.IsEnum)
                    writer.WriteLine($"{pi.Name}:{Convert.ToInt32(value)}");
                else
                    writer.WriteLine($"{pi.Name}: {value}");
                
            }
        }

        protected abstract bool IsIgnored(string propertyName);
    }
}
